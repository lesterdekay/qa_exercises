/**
 * Adds a watcher to all of the supplied issues.
 * If there is partial success, the issues which we can modify will
 * be modified and the ones we cannot will be returned in an ArrayList.
 * @param issues the list of issues to update
 * @param currentUser the user to run the operation as
 * @param watcher the watcher to add to the issues
 * @return an ArrayList<Issue> containing the issues that could not be modified
*/
public ArrayList<Issue> addWatcherToAll (final ArrayList<Issue> issues, final User currentUser, final User watcher) {
    ArrayList<Issue> successfulIssues = new ArrayList<Issue> ();
    ArrayList<Issue> failedIssues = new ArrayList<Issue> ();
    for (Issue issue : issues) {
        if (canWatchIssue(issue, currentUser, watcher)) {
            successfulIssues.add (issue);
        }
        else {
            failedIssues.add (issue);
        }
    }
/*  Original line below followed by the reason for change
    if (successfulIssues.isEmpty()) {
    reason for change: the list should only be modified if the list is NOT empty */
    if (!successfulIssues.isEmpty()) {
        watcherManager.startWatching (currentUser, successfulIssues);
    }
    return failedIssues;
}
private boolean canWatchIssue (Issue issue, User currentUser, User watcher) {
/*  Original line below followed by the reason for change
    if (currentUser.equals(watcher) || currentUser.getHasPermissionToModifyWatchers()) {
    reason for change: whether the current user is the same as the watcher should have no relevance to the 
    permissions "modify watchers" or "watching allowed" */
    if (currentUser.getHasPermissionToModifyWatchers()) {
        return issue.getWatchingAllowed (watcher);
    }
/*  Original line below followed by the reason for change
    return true;
    reason for change: if the current user cannot modify the issue then the method should return false */
    return false;
}
